function edit(id) {
	$("#vehicleModalLabel").text("Editar Vehículo");
	$("#confirm").val(id);
	$.ajax({
		type: 'GET',
		url: "/vehicles/get/" + id,
		contentType: "application/json; charset=utf-8",
		dataType: 'json',
		success: function(response){
			$("#brand").val(response.brand.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	    	$("#model").val(response.model);
	    	$("#plate").val(response.plate.toUpperCase()),
	    	$("#licenseRequired").val(response.licenseRequired.toUpperCase());
	    	$("#vehicleModal").modal('show');
		},
		error: function (xhr, ajaxOptions, thrownError){
			toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
            return false;
        }
	});
}

function reset() {
	$("#brand").val("");
	$("#model").val("");
	$("#plate").val(""),
	$("#licenseRequired").val("");
}

function vehicleDelete(id, brand, plate) {
	$("#vPTextBrand").append(brand.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	$("#vPTextPlate").append(plate.toUpperCase());
	$("#confirmDelete").val(id);
	$("#vehicleModalDelete").modal('show');
}

$( document ).ready(function() {
	findAll();
	
    $("#confirm").click(function() {
    	if ($("#vehicleForm").valid()) {
    		$("#confirm").val() == 0 ? store() : update();
    	}
	});
    
    $("#vCreate").click(function() {
    	reset();
    	$("#vehicleModalLabel").text("Registrar Vehículo");
    	$("#confirm").val(0);
    	$("#vehicleModal").modal('show');
	});
    
    $("#confirmDelete").click(function() {
    	vehicleDeleteConfirm($("#confirmDelete").val());
	});
    
    function findAll() {
    	$("#tBodyV").empty();
	
		$.ajax({
			type: 'GET',
			url: "/vehicles/list",
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				if (response.length > 0) {
					var html = "";
					
					$.each(response, function( index, vehicle ) {
						html += '<tr>';
						html += '<td scope="row">' + (index + 1) + '</td>';
						html += '<td>' + vehicle.brand.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) + '</td>';
						html += '<td>' + vehicle.model + '</td>';
						html += '<td>' + vehicle.plate.toUpperCase() + '</td>';
						html += '<td>' + vehicle.licenseRequired.toUpperCase() + '</td>';
						html += '<td>' +
						 	'<button type="button" class="btn btn-warning" onclick="edit(' + vehicle.id + ');">' +
						 		'<i class="fas fa-pencil-alt"></i>' +
					 		'</button>' +
					 		'<a href="/vehicles/show/' + vehicle.id + '" class="btn btn-primary">' +
						 		'<i class="fas fa-eye"></i>' +
					 		'</a>' +
					 		'<button type="button" class="btn btn-danger" onclick="vehicleDelete(' + vehicle.id + ',\'' + vehicle.brand + '\',\'' + vehicle.plate + '\');">' +
						 		'<i class="fas fa-trash-alt"></i>' +
					 		'</button>' +
					 		
					 	 '</td>';
						html += '</tr>';
					  });
					
					$("#tBodyV").append(html);
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
		});
    }
    
    function store() {
    	var vehicle = { 
    		brand: $("#brand").val(),
    		model: $("#model").val(),
    		plate: $("#plate").val(),
    		licenseRequired: $("#licenseRequired").val()
    	};
    	
    	$.ajax({
			type: 'POST',
			url: "/vehicles/store",
			data: JSON.stringify(vehicle),
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				toastr.success("Vehículo registrado con éxito");
				findAll();
				$("#vehicleModal").modal('hide');
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
    
    function update() {
    	var vehicle = {
			id: $("#confirm").val(), 
    		brand: $("#brand").val(),
    		model: $("#model").val(),
    		plate: $("#plate").val(),
    		licenseRequired: $("#licenseRequired").val()
    	};
    	
    	$.ajax({
			type: 'PUT',
			url: "/vehicles/update",
			data: JSON.stringify(vehicle),
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				toastr.success("Vehículo actualizado con éxito");
				findAll();
				$("#vehicleModal").modal('hide');
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
    
    function vehicleDeleteConfirm(id) {
    	$.ajax({
    		type: 'DELETE',
    		url: "/vehicles/delete/" + id,
    		contentType: "application/json; charset=utf-8",
    		dataType: 'json',
    		success: function(response){
    			toastr.success("Vehículo eliminado con éxito");
    			findAll();
    			$("#vehicleModalDelete").modal('hide');
    		},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
});