$(document).ready(function () {
	
	var $form = $("#vehicleForm");
	
	$.validator.addMethod("noSpace", function(value, element) {
		return value == "" || value.trim().length != 0
	}, "Los espacios en blanco no son permitidos");
	
	if ($form.length) {
		$form.validate({
			rules: {
				brand: {
					required: true,
					noSpace: true
				},
				model: {
					required: true,
					noSpace: true
				},
				plate: {
					required: true,
					noSpace: true
				},
				licenseRequired: {
					required: true
				}
			},
			messages: {
				brand: {
					required: "El campo Marca es obligatorio."
				},
				model: {
					required: "El campo Modelo es obligatorio."
				},
				plate: {
					required: "El campo Placa es obligatorio."
				},
				licenseRequired: {
					required: "Debe seleccionar uno de los tipos de licencia de la lista."
				},
			}
		});
	}
});