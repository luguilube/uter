$(document).ready(function () {
	
	var $form = $("#tripeForm");
	
	$.validator.addMethod("noSpace", function(value, element) {
		return value == "" || value.trim().length != 0
	}, "Los espacios en blanco no son permitidos");
	
	if ($form.length) {
		$form.validate({
			rules: {
				datepicker: {
					required: true,
					noSpace: true
				},
				selectVehicule: {
					required: true
				},
				selectDriver: {
					required: true
				}
			},
			messages: {
				datepicker: {
					required: "El campo Fecha es obligatorio."
				},
				selectVehicule: {
					required: "Debe seleccionar un Vehículo de la lista."
				},
				selectDriver: {
					required: "Debe seleccionar un Conductor de la lista."
				}
			}
		});
	}
});