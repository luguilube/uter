function reset() {
	$("#datepicker").val("");
	$("#selectVehicule").empty();
	$('#selectVehicule').prop('disabled', true);
	$("#selectDriver").empty();
	$('#selectDriver').prop('disabled', true);
}

$( document ).ready(function() {
	setupDatePicker();
	findAll();
			
	$("#datepicker").change(function(){
		  if ($("#datepicker").val().length > 0) {
			  getVehicules();
		  } else {
			  reset();
		  }
	});
	
	$("#selectVehicule").change(function(){
		  if ($("#selectVehicule").val() != "") {
			  getDrivers();
		  } else {
			  reset();
		  }
	});
	
    $("#confirm").click(function() {
    	if ($("#tripeForm").valid()) {
    		store();
    	}
	});
    
    $("#tCreate").click(function() {
    	reset();
    	$("#tripeModalLabel").text("Registrar Vehículo");
    	$("#confirm").val(0);
    	$("#tripeModal").modal('show');
	});
    
    function findAll() {
    	$("#tBodyT").empty();
	
		$.ajax({
			type: 'GET',
			url: "/tripes/list",
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				
				if (response.length > 0) {
					var html = "";
					
					$.each(response, function( index, tripe ) {
						html += '<tr>';
						html += '<td scope="row">' + (index + 1) + '</td>';
						html += '<td>' + tripe.vehicle.brand.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) + '</td>';
						html += '<td>' + tripe.vehicle.plate.toUpperCase() + '</td>';
						html += '<td>' + tripe.driver.name.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) +
							' ' + tripe.driver.surname.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) +
							'</td>';
						html += '<td>' + tripe.date + '</td>';
						html += '</tr>';
					  });
					
					$("#tBodyT").append(html);
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
		});
    }
    
    function store() {
    	var tripe = { 
			dateString: $("#datepicker").val(),
    		vehicleId: $("#selectVehicule").val(),
    		driverId: $("#selectDriver").val()
    	};
    	
    	$.ajax({
			type: 'POST',
			url: "/tripes/store",
			data: JSON.stringify(tripe),
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				$("#tripeModal").modal("hide");
				toastr.success("Viaje registrado con éxito");
				reset();
				findAll();
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }  
    
    function setupDatePicker() {
    	 $.datepicker.regional['es'] = {
    			 closeText: 'Cerrar',
    			 prevText: '< Ant',
    			 nextText: 'Sig >',
    			 currentText: 'Hoy',
    			 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    			 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    			 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    			 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    			 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    			 weekHeader: 'Sm',
    			 dateFormat: 'dd/mm/yy',
    			 firstDay: 1,
    			 isRTL: false,
    			 showMonthAfterYear: false,
    			 yearSuffix: ''
    			 };
    			 $.datepicker.setDefaults($.datepicker.regional['es']);
    			 
		$("#datepicker").datepicker({
	        dateFormat: 'dd-mm-yy', // date format
	        minDate: 0
		});

    }
    
    function getVehicules() {
    	$("#selectVehicule").empty();
    	
    	$.ajax({
			type: 'GET',
			url: "/tripes/get-vehicles/" + $("#datepicker").val(),
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				if (response.length > 0) {
					var html = "";
					
					$.each(response, function( index, vehicle ) {
						html += '<option value="' + vehicle.id + '">' + 
						vehicle.brand.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) + " - " + vehicle.model + '</option>';
				    });
					
					$("#selectVehicule").append(html);
					$("#selectVehicule").val("");
					$('#selectVehicule').prop('disabled', false);
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
    
    function getDrivers() {
    	$("#selectDriver").empty();
    	
    	$.ajax({
			type: 'GET',
			url: "/tripes/get-drivers/" + $("#datepicker").val() + "/" + $("#selectVehicule").val(),
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				if (response.length > 0) {
					var html = "";
					
					$.each(response, function( index, driver ) {
						html += '<option value="' + driver.id + '">' + 
						driver.name.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) + " " + 
						driver.surname.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) + '</option>';
				    });
					
					$("#selectDriver").append(html);
					$("#selectDriver").val("");
					$('#selectDriver').prop('disabled', false);
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
});