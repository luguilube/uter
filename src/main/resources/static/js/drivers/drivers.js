function edit(id) {
	$("#driverModalLabel").text("Editar Conductor");
	$("#confirm").val(id);
	$.ajax({
		type: 'GET',
		url: "/drivers/get/" + id,
		contentType: "application/json; charset=utf-8",
		dataType: 'json',
		success: function(response){
			$("#name").val(response.name.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	    	$("#surname").val(response.surname.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	    	$("#license").val(response.license.toUpperCase());
	    	$("#driverModal").modal('show');
		},
		error: function (xhr, ajaxOptions, thrownError){
			toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
            return false;
        }
	});
}

function reset() {
	$("#name").val("");
	$("#surname").val("");
	$("#license").val("");
}

function driverDelete(id, name, surname) {
	$("#dPTextName").append(name.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	$("#dPTextSurname").append(surname.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	$("#confirmDelete").val(id);
	$("#driverModalDelete").modal('show');
}

$( document ).ready(function() {
	findAll();
	
    $("#confirm").click(function() {
    	if ($("#driverForm").valid()) {
        	$("#confirm").val() == 0 ? store() : update();
    	}
	});
    
    $("#pCreate").click(function() {
    	reset();
    	$("#driverModalLabel").text("Registrar Conductor");
    	$("#confirm").val(0);
    	$("#driverModal").modal('show');
	});
    
    $("#confirmDelete").click(function() {
    	driverDeleteConfirm($("#confirmDelete").val());
	});
    
    function findAll() {
    	$("#tBodyP").empty();
	
		$.ajax({
			type: 'GET',
			url: "/drivers/list",
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				if (response.length > 0) {
					var html = "";
					
					$.each(response, function( index, driver ) {
						html += '<tr>';
						html += '<td scope="row">' + (index + 1) + '</td>';
						html += '<td>' + driver.name.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) + '</td>';
						html += '<td>' + driver.surname.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }) + '</td>';
						html += '<td>' + driver.license.toUpperCase() + '</td>';
						html += '<td>' +
						 	'<button type="button" class="btn btn-warning" onclick="edit(' + driver.id + ');">' +
						 		'<i class="fas fa-pencil-alt"></i>' +
					 		'</button>' +
					 		'<a href="/drivers/show/' + driver.id + '" class="btn btn-primary">' +
						 		'<i class="fas fa-eye"></i>' +
					 		'</a>' +
					 		'<button type="button" class="btn btn-danger" onclick="driverDelete(' + driver.id + ',\'' + driver.name + '\',\'' + driver.surname + '\');">' +
						 		'<i class="fas fa-trash-alt"></i>' +
					 		'</button>' +
					 	 '</td>';
						html += '</tr>';
					  });
					
					$("#tBodyP").append(html);
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
		});
    }
    
    function store() {
    	var driver = { 
    		name: $("#name").val(),
    		surname: $("#surname").val(),
    		license: $("#license").val()
    	};
    	
    	$.ajax({
			type: 'POST',
			url: "/drivers/store",
			data: JSON.stringify(driver),
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				toastr.success("Conductor registrado con éxito");
				findAll();
				$("#driverModal").modal('hide');
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
    
    function update() {
    	var driver = {
			id: $("#confirm").val(), 
    		name: $("#name").val(),
    		surname: $("#surname").val(),
    		license: $("#license").val()
    	};
    	
    	$.ajax({
			type: 'PUT',
			url: "/drivers/update",
			data: JSON.stringify(driver),
			contentType: "application/json; charset=utf-8",
			dataType: 'json',
			success: function(response){
				toastr.success("Conductor actualizado con éxito");
				findAll();
				$("#driverModal").modal('hide');
			},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
    
    function driverDeleteConfirm(id) {
    	$.ajax({
    		type: 'DELETE',
    		url: "/drivers/delete/" + id,
    		contentType: "application/json; charset=utf-8",
    		dataType: 'json',
    		success: function(response){
    			toastr.success("Conductor eliminado con éxito");
    			findAll();
    			$("#driverModalDelete").modal('hide');
    		},
			error: function (xhr, ajaxOptions, thrownError){
				toastr.error("Error en el servidor: "+xhr.responseText+", Error: "+thrownError);
	            return false;
	        }
    	});
    }
});