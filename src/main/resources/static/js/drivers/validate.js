$(document).ready(function () {
	
	var $form = $("#driverForm");
	
	$.validator.addMethod("noSpace", function(value, element) {
		return value == "" || value.trim().length != 0
	}, "Los espacios en blanco no son permitidos");
	
	if ($form.length) {
		$form.validate({
			rules: {
				name: {
					required: true,
					noSpace: true
				},
				surname: {
					required: true,
					noSpace: true
				},
				license: {
					required: true
				}
			},
			messages: {
				name: {
					required: "El campo Nombre es obligatorio."
				},
				surname: {
					required: "El campo Apellido es obligatorio."
				},
				license: {
					required: "Debe seleccionar uno de los tipos de licencia de la lista."
				}
			}
		});
	}
});