package com.hiberus.app.uter.exam.models.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.hiberus.app.uter.exam.models.entities.Tripe;

public interface ITripeRepository extends CrudRepository<Tripe, Long>{
	public List<Tripe> findAll();
	public List<Tripe> findAllByDate(Date date);
	
	@Query("SELECT t FROM Tripe t WHERE t.vehicle.id = ?1")
	public List<Tripe> findTripsByVehicleId(Long id);
	
	@Query("SELECT t FROM Tripe t WHERE t.driver.id = ?1")
	public List<Tripe> findTripsByDriverId(Long id);
}
