package com.hiberus.app.uter.exam.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hiberus.app.uter.exam.models.entities.Vehicle;
import com.hiberus.app.uter.exam.models.repositories.IVehicleRepository;

@Service
public class VehicleServiceImpl implements IVehicleService {
	
	@Autowired
	private IVehicleRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public Vehicle findById(Long id) {
		return repository.findById(id).orElse(null);
	}


	@Override
	@Transactional(readOnly = true)
	public List<Vehicle> findAll() {
		return repository.findAll();
	}
	
	@Override
	@Transactional
	public Vehicle save(Vehicle vehicle) {
		return repository.save(vehicle);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		repository.deleteById(id);
	}

}
