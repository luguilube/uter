package com.hiberus.app.uter.exam.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hiberus.app.uter.exam.models.entities.Driver;
import com.hiberus.app.uter.exam.models.repositories.IDriverRepository;

@Service
public class DriverServiceImpl implements IDriverService{
	
	@Autowired
	private IDriverRepository repository;

	@Override
	@Transactional(readOnly = true)
	public Driver findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Driver> findAll() {
		return repository.findAll();
	}

	@Override
	@Transactional
	public Driver save(Driver driver) {
		return repository.save(driver);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		repository.deleteById(id);	
	}
}
