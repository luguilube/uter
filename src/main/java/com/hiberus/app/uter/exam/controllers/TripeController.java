package com.hiberus.app.uter.exam.controllers;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException.BadRequest;

import com.hiberus.app.uter.exam.models.entities.Driver;
import com.hiberus.app.uter.exam.models.entities.Tripe;
import com.hiberus.app.uter.exam.models.entities.Vehicle;
import com.hiberus.app.uter.exam.models.services.IDriverService;
import com.hiberus.app.uter.exam.models.services.ITripeService;
import com.hiberus.app.uter.exam.models.services.IVehicleService;

@Controller
@RequestMapping("/tripes")
public class TripeController {
	
	private static final Logger log = LoggerFactory.getLogger(TripeController.class);
	

	private ITripeService service;
	private IVehicleService vehicleService;
	private IDriverService driverService;
	
	@Autowired
	public TripeController(ITripeService service, IVehicleService vehicleService, IDriverService driverService) {
		this.service = service;
		this.vehicleService = vehicleService;
		this.driverService = driverService;
	}

	@GetMapping("")
	public String index() {	
		return "tripes/tripes";
	}
	
	@GetMapping("/list")
	@ResponseBody
	public ResponseEntity<?> list() {
		try {
			List<Tripe> tripes = service.findAll();
			
			tripes.stream()
			.map(tripe -> {	
				Vehicle vehicle = new Vehicle(); 
				Driver driver = new Driver();
				
				vehicle.setBrand(tripe.getVehicle().getBrand());
				vehicle.setPlate(tripe.getVehicle().getPlate());
				
				tripe.setVehicle(vehicle);
				
				driver.setName(tripe.getDriver().getName());
				driver.setSurname(tripe.getDriver().getSurname());
				
				tripe.setDriver(driver);	
				
				return tripe;
				
			}).collect(Collectors.toList());
			
			return ResponseEntity.ok().body(tripes);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
		} catch (Exception e1) {
			log.error("Exception {}", e1);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/get-vehicles/{date}")
	@ResponseBody
	public ResponseEntity<?> getVehicles(@PathVariable String date) {
		try {
			Format format = new SimpleDateFormat("dd-MM-yyyy");
			Date dateConverted = (Date) format.parseObject(date);
			List<Tripe> tripesScheduled = service.findAllByDate(dateConverted);
			
			List<Vehicle> vehicles = vehicleService.findAll();
			
			vehicles = vehicles.stream()
			.filter(vehicle -> {
				boolean found = false;
				for (Tripe tripe: tripesScheduled) {
					if (tripe.getVehicle().getId() == vehicle.getId()) {
						found = true;
						break;
					}
				}

				return found ? false : true;
			}).collect(Collectors.toList());
			
			return ResponseEntity.ok().body(vehicles);
		} catch (BadRequest e) {
			log.error("BadRequest {}", e);
		} catch (InternalError e1) {
			log.error("InternalError {}", e1);
		} catch (Exception e2) {
			log.error("Exception {}", e2);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/get-drivers/{date}/{vehicleId}")
	@ResponseBody
	public ResponseEntity<?> getDrivers(@PathVariable String date, @PathVariable Long vehicleId) {
		try {
			Format format = new SimpleDateFormat("dd-MM-yyyy");
			Date dateConverted = (Date) format.parseObject(date);
			List<Tripe> tripesScheduled = service.findAllByDate(dateConverted);
			
			List<Driver> drivers = driverService.findAll();
			
			Vehicle vehicleIncoming = vehicleService.findById(vehicleId);
			
			drivers = drivers.stream()
			.filter(driver -> {
				boolean found = false;
				
				for (Tripe tripe: tripesScheduled) {
					if (tripe.getDriver().getId() == driver.getId()) {
						found = true;
						break;
					}
				}

				return !found && driver.getLicense().equalsIgnoreCase(vehicleIncoming.getLicenseRequired()) ? true : false;
			}).collect(Collectors.toList());
			
			return ResponseEntity.ok().body(drivers);
		} catch (BadRequest e) {
			log.error("BadRequest {}", e);
		} catch (InternalError e1) {
			log.error("InternalError {}", e1);
		} catch (Exception e2) {
			log.error("Exception {}", e2);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping("/store")
	@ResponseBody
	public ResponseEntity<?> store(@RequestBody Tripe tripe) {
		try {
			Format format = new SimpleDateFormat("dd-MM-yyyy");
			Date dateConverted = (Date) format.parseObject(tripe.getDateString());
			
			Vehicle vehicle = vehicleService.findById(tripe.getVehicleId());
			Driver driver = driverService.findById(tripe.getDriverId());
			
			tripe.setDate(dateConverted);
			tripe.setVehicle(vehicle);
			tripe.setDriver(driver);
			
			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(tripe));
		}  catch (BadRequest e) {
			log.error("BadRequest {}", e);
		} catch (InternalError e1) {
			log.error("InternalError {}", e1);
		} catch (Exception e2) {
			log.error("Exception {}", e2);
		}

		return ResponseEntity.noContent().build();
	}

}
