package com.hiberus.app.uter.exam.models.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hiberus.app.uter.exam.models.entities.Tripe;
import com.hiberus.app.uter.exam.models.repositories.ITripeRepository;

@Service
public class TripeServiceimpl implements ITripeService {
	
	@Autowired
	private ITripeRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Tripe> findAll() {
		return repository.findAll();
	}

	@Override
	@Transactional
	public Tripe save(Tripe tripe) {
		return repository.save(tripe);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Tripe> findAllByDate(Date date) {
		return repository.findAllByDate(date);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Tripe> findTripsByVehicleId(Long id) {
		return repository.findTripsByVehicleId(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Tripe> findTripsByDriverId(Long id) {
		return repository.findTripsByDriverId(id);
	}
}
