package com.hiberus.app.uter.exam.models.services;

import java.util.Date;
import java.util.List;

import com.hiberus.app.uter.exam.models.entities.Tripe;

public interface ITripeService {
	public List<Tripe> findAll();
	public Tripe save(Tripe tripe);
	public List<Tripe> findAllByDate(Date date);
	public List<Tripe> findTripsByVehicleId(Long id);
	public List<Tripe> findTripsByDriverId(Long id);
}
