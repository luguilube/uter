package com.hiberus.app.uter.exam.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hiberus.app.uter.exam.models.entities.Driver;
import com.hiberus.app.uter.exam.models.services.IDriverService;
import com.hiberus.app.uter.exam.models.services.ITripeService;

@Controller
@RequestMapping("/drivers")
public class DriverController {
	private static final Logger log = LoggerFactory.getLogger(DriverController.class);
	
	private IDriverService service;
	private ITripeService tripeService;
	
	@Autowired
	public DriverController(IDriverService service, ITripeService tripeService) {
		this.service = service;
		this.tripeService = tripeService;
	}

	@GetMapping("")
	public String index() {
		return "drivers/drivers";
	}
	
	@GetMapping("/list")
	@ResponseBody
	public ResponseEntity<?> list() {
		
		try {
			return ResponseEntity.ok().body(service.findAll());
		} catch (InternalError e) {
			log.error("InternalError {}", e);
		} catch (Exception e1) {
			log.error("Exception {}", e1);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/show/{id}")
	public String show(@PathVariable Long id, Model model) {
		
		Driver driver = null;
		try {
			driver = service.findById(id);
			
			if (driver == null) {
				return "errors/404";
			}
			
			model.addAttribute("driver", driver);
			model.addAttribute("tripes", tripeService.findTripsByDriverId(id));
			
		} catch (InternalError e) {
			log.error("InternalError {}", e);
		} catch (Exception e1) {
			log.error("Exception {}", e1);
		}
		
		return "drivers/show";
	}
	
	@PostMapping("/store")
	@ResponseBody
	public ResponseEntity<?> store(@Valid @RequestBody Driver driver, BindingResult result) {
		
		try {
			if (result.hasErrors()) {
				return validar(result);
			}
			
			driver.setName(driver.getName().toLowerCase());
			driver.setSurname(driver.getSurname().toLowerCase());
			
			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(driver));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
		} catch (Exception e1) {
			log.error("Exception {}", e1);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/get/{id}")
	@ResponseBody
	public ResponseEntity<?> get(@PathVariable Long id) {
		
		try {
			return ResponseEntity.ok().body(service.findById(id));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
		} catch (Exception e1) {
			log.error("Exception {}", e1);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/update")
	@ResponseBody
	public ResponseEntity<?> update(@Valid @RequestBody Driver driver, BindingResult result) {
		
		try {
			if (result.hasErrors()) {
				return validar(result);
			}
			
			driver.setName(driver.getName().toLowerCase());
			driver.setSurname(driver.getSurname().toLowerCase());
			
			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(driver));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
		} catch (Exception e1) {
			log.error("Exception {}", e1);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseBody
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		try {
			service.deleteById(id);
			
			return ResponseEntity.ok().body(true);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
		} catch (Exception e1) {
			log.error("Exception {}", e1);
		}
		
		return ResponseEntity.noContent().build();
	}
	
	protected ResponseEntity<?> validar(BindingResult result) {
		Map<String, Object> errores = new HashMap<>();
		result.getFieldErrors().forEach(err -> {
			errores.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
		});
		
		return ResponseEntity.badRequest().body(errores);
	}
}
