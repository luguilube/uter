package com.hiberus.app.uter.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UterExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(UterExamApplication.class, args);
	}

}
