package com.hiberus.app.uter.exam.models.services;

import java.util.List;

import com.hiberus.app.uter.exam.models.entities.Driver;

public interface IDriverService {
	public Driver findById(Long id);
	public List<Driver> findAll();
	public Driver save(Driver driver);
	public void deleteById(Long id);
}
