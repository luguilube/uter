package com.hiberus.app.uter.exam.models.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "vehicles")
public class Vehicle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String brand;
	
	@NotEmpty
	private String model;
	
	@NotEmpty
	private String plate;
	
	@NotEmpty
	@Size(max = 1)
	@Column(name = "license_required", length = 1)
	private String licenseRequired;
	
	@JsonIgnoreProperties(value = {"vehicle", "driver"}, allowSetters = true)
	@OneToMany(mappedBy= "vehicle", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Tripe> tripes;

	
	public Vehicle() {
		this.tripes = new ArrayList<>();
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getBrand() {
		return brand;
	}



	public void setBrand(String brand) {
		this.brand = brand;
	}



	public String getModel() {
		return model;
	}



	public void setModel(String model) {
		this.model = model;
	}



	public String getPlate() {
		return plate;
	}



	public void setPlate(String plate) {
		this.plate = plate;
	}



	public String getLicenseRequired() {
		return licenseRequired;
	}



	public void setLicenseRequired(String licenseRequired) {
		this.licenseRequired = licenseRequired;
	}

	public List<Tripe> getTripes() {
		return tripes;
	}



	public void setTripes(List<Tripe> tripes) {
		this.tripes = tripes;
	}


	public void addTripe(Tripe tripe) {
		this.tripes.add(tripe);
	}

	public void removeTripe(Tripe tripe) {
		this.tripes.remove(tripe);
	}

	@Override
	public String toString() {
		return "Vehicle{" +
                "id=" + id +
                ", brand='" + brand + "'" +
                ", model='" + model + "'" +
                ", plate='" + plate + "'" +
                ", licenseRequired='" + licenseRequired + "'" +
                '}';
	}
	
	
}
