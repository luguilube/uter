package com.hiberus.app.uter.exam.models.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "drivers")
public class Driver {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String name;
	
	@NotEmpty
	private String surname;
	
	@NotEmpty
	@Size(max = 1)
	private String license;
	
	@JsonIgnoreProperties(value = {"vehicle", "driver"}, allowSetters = true)
	@OneToMany(mappedBy= "vehicle", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Tripe> tripes;
	
	public Driver() {
		this.tripes = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}
	
	public List<Tripe> getTripes() {
		return tripes;
	}

	public void setTripes(List<Tripe> tripes) {
		this.tripes = tripes;
	}

	public void addTripe(Tripe tripe) {
		this.tripes.add(tripe);
	}

	public void removeTripe(Tripe tripe) {
		this.tripes.remove(tripe);
	}

	@Override
	public String toString() {
		return "Driver{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", surname='" + surname + "'" +
                ", license='" + license + "'" +
                '}';
	}
}
