package com.hiberus.app.uter.exam.models.services;

import java.util.List;

import com.hiberus.app.uter.exam.models.entities.Vehicle;

public interface IVehicleService {
	public Vehicle findById(Long id);
	public List<Vehicle> findAll();
	public Vehicle save(Vehicle vehicle);
	public void deleteById(Long id);
}
