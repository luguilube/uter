package com.hiberus.app.uter.exam.models.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.hiberus.app.uter.exam.models.entities.Driver;

public interface IDriverRepository extends CrudRepository<Driver, Long>{
	public Optional<Driver> findById(Long id);
	public List<Driver> findAll();
}
