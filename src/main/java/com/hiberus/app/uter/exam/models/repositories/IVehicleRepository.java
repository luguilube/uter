package com.hiberus.app.uter.exam.models.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.hiberus.app.uter.exam.models.entities.Vehicle;

public interface IVehicleRepository extends CrudRepository<Vehicle, Long>{
	public Optional<Vehicle> findById(Long id);
	public List<Vehicle> findAll();
}
