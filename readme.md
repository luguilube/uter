## USTER

USTER es una app que permite el agendamiento de viajes, para los cuales se requiere seleccionar un vehículo y un conductor.
Este proyecto es una versión web USTER la cual permite la gestion completa de vehículos y conductores por medio de una interfaz agradable e intuitiva para los usuarios. También permite visualizar el listado de viajes agendados y el registro de nuevos viajes.

## Heramientas Utilizadas

Este proyecto fue desarrollado con el framework Java [Spring Boot 2](https://spring.io/projects/spring-boot), la lógica de negocio y consultas a base de datos se realizó en el Backend con Java haciendo uso de tecnologías como JPA, Spring Boot Validation y en el Frontend se hacen uso de las tecnologías Thymeleaf, JQuery, JQuery validation, Toastr; Bootstrap 4 para el uso de clases y estilos, HTML y CSS. y una base de datos MariaDB(Mysql).
Este proyecto se ejecuta por defecto en el puerto 8080.

## Alojado en un Repositorio GIT

- El proyecto se encuentra alojado en un repositorio GIT en [Bitbucket](https://bitbucket.org/luguilube/uter/src/master/).

## Instalación

Los pasos para poder instalar el proyecto son: 

- Descargar e instalar GIT (esto dependera del sistema operativo el cual se posea).
- Tener instalado en su version 11 o superior [Java](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html).
- En una terminal a elección, situarse en la ruta donde se desea descargar el proyecto y ejecutar el comando dependiendo de la forma:
   * HTTPS: git clone https://bitbucket.org/luguilube/uter.git  
- Crear una base de datos vacía MariaDB (fork de MySQL) o MySQL y con el nombre uster, para hacer esto primeramente se debe tener MariaDB instalado y luego se puede hacer mediante la terminal o támbien con algún manejador de bases de datos como DBeaver, HeydiSQL, WorkBench o el que de su agrado. 
  La Generacion de las tablas se hara mediante Spring Boot automaticamente.
- Luego dependiendo de si se hace por terminal o por medio de un IDE:
  * Se debe situar en donde fue clonado el proyecto y ejecutar el comando: mvn install
  * Se importa el proyecto en un IDE tal como Spring Boot Tool Suit (STS) o elcipse con el plugin de STS, o IntelliJ y se deben instalar todas las dependencias del proyecto, si no es asi, segun el IDE seleccionado boton derecho sobre el proyecto > maven > Update Project
- Acceder a la url del proyecto http://localhost:8080

## Sugerencias y Correcciones

Ante cualquier error, sugerencias, o comentario, por favor indicarme via correo a elluisluna@gmail.com, estare muy agradecido.  
